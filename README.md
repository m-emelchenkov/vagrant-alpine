# Alpine Linux Vagrant Box for Parallels VM

## Build
```
cd Packer
packer build alpine-3.7.json
``` 

## Install
```
cd Packer
vagrant box add --name m-emelchenkov/alpine37-vanilla file://./build/vagrant_parallels_alpine-3.7-x86-64-vanilla.box
```

## Use
```
vagrant init m-emelchenkov/alpine37-vanilla
vagrant up
vagrant ssh
sudo apk update
```

## Known issues
- Parallels Tools does not work under `-hardened` and `-virthardened` kernels.
- No custom network configurations are supported. 

## Licenses
[alpine-pkg-glibc](https://github.com/sgerrand/alpine-pkg-glibc) package under [LGPL-2.1](https://tldrlegal.com/license/gnu-lesser-general-public-license-v2.1-(lgpl-2.1)) license.

[dkms](https://github.com/dell/dkms) package under [GPL-2.0](https://tldrlegal.com/license/gnu-general-public-license-v2) license.

Copyright © 2018 [Mikhail Emelchenkov](https://www.Emelchenkov.pro). Source code is distributed under [CDDL-1.0](https://tldrlegal.com/license/common-development-and-distribution-license-\(cddl-1.0\)-explained) open source license.