#!/bin/ash
set -eux

# Record image build time 
date > /etc/vagrant_box_build_time

# `vagrant reload` compatibility
install -m 755 shutdown /sbin/shutdown

# `motd` message
install -m 644 motd /etc/motd

# Create `vagrant` user
adduser -D vagrant
echo "vagrant:vagrant" | chpasswd

# Allow passwordless `sudo` for `vagrant` user
apk add sudo
sed -i 's/^\s*#*\s*%wheel ALL=(ALL) NOPASSWD: ALL.*/%wheel ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers
adduser vagrant wheel

# Download insecure SSH key
mkdir -pm 700 /home/vagrant/.ssh
wget -qO /home/vagrant/.ssh/authorized_keys 'https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub'
chmod 600 /home/vagrant/.ssh/authorized_keys 
chown -R vagrant:vagrant /home/vagrant/.ssh