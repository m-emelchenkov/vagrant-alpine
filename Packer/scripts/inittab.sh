#!/bin/ash
set -eux

# Fix kernel log flooding (comments ttyS0 lines)
sed -ri 's/^\s*ttyS0(.*)/#ttyS0\1/g' /etc/inittab