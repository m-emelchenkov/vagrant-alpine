#!/bin/ash
set -eux

# Disable remote root login
sed -i 's/^PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config

# Make SSH connection faster
sed -i 's/^\s*#*\s*UseDNS.*/UseDNS no/' /etc/ssh/sshd_config