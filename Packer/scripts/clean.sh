#!/bin/ash
set -eux

# Clean install files
rm -rf ~/*

# Clean package cache
rm -rf /var/cache/apk/*

# Clean log files
find /var/log -type f -exec truncate -s 0 {} \;
rm -f /var/log/parallels*

# Compress disk partitions
set +e
dd if=/dev/zero of=/boot/EMPTY; rm -f /boot/EMPTY
dd if=/dev/zero of=/EMPTY; rm -f /EMPTY
swap_uuid="$(/sbin/blkid -o value -l -s UUID -t TYPE=swap)"
case $? in
    2|0) ;;
    *)   exit 1 ;;
esac
if [ $swap_uuid ]; then
    swap_part="$(readlink -f /dev/disk/by-uuid/$swap_uuid)"
    if [ $swap_part ]; then
        /sbin/swapoff $swap_part
        dd if=/dev/zero of=$swap_part bs=1M
        /sbin/mkswap -U $swap_uuid $swap_part
    fi
fi
set -e

sync