#!/bin/ash
set -eux

# Add Community repository
echo "http://dl-cdn.alpinelinux.org/alpine/v3.7/community" >> /etc/apk/repositories

# Update package cache & upgrade outdated packages
apk upgrade -U --available