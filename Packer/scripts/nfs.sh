#!/bin/ash
set -eux

# Install NFS server to support NFS synced folders
apk add nfs-utils

# Enable NFS server auto-start
rc-update add rpcbind