#!/bin/ash
set -eux

# Install dev dependencies
apk add gcc make
# Install dependencies
apk add linux-vanilla-dev
apk add --allow-untrusted ./glibc-2.26-r0.apk
apk add --allow-untrusted ./dkms-2.5-r0.apk

# Patch Parallels Tools (tested on 13.2.0.43213)
mount -t iso9660 -o loop prl-tools-lin.iso /media/cdrom
cp -r /media/cdrom ./prl-tools
umount /media/cdrom
cd prl-tools
    patch -p1 < ../prl-tools-patch.diff
cd ..

# Install Parallels Tools
./prl-tools/install --install-unattended --verbose

# Enable Parallels Tools auto-start
install -m 755 prltoolsd.start /etc/local.d/
rc-update add local

# Uninstall dev dependencies
apk del gcc make