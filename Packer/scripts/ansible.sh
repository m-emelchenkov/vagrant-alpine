#!/bin/ash
set -eux

# Install packages needed for Ansible provision
apk add python2 py2-simplejson py2-openssl rsync